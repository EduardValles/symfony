# SYMFONY

## CAPTURES

#### LOCAL

![alt text](https://bytebucket.org/EduardValles/symfony/raw/433a0aae5cae14794ec56de940825aa77e916300/sym_local.png "LOCAL")

#### PRODUCCIÓ

![alt text](https://bytebucket.org/EduardValles/symfony/raw/433a0aae5cae14794ec56de940825aa77e916300/sym_pro.png "PRODUCCIÓ")


## REFERÈNCIES (LINKOGRAFIA)


[Problemes de permisos](http://symfony.es/documentacion/como-solucionar-el-problema-de-los-permisos-de-symfony2/)


[Curs Udemy Symfony 3 PROD](https://www.udemy.com/symfony-3-en-produccion-subir-y-publicar-proyectos-web/learn/v4/overview)


[Guía al moodle](https://campus.iam.cat/moodle/mod/page/view.php?id=66185)


[Documentació oficial Symfony](https://symfony.com/doc/3.4/deployment.html)


[Documentació oficial Symfony](https://symfony.com/doc/3.4/deployment.html)


## ERRORS TROBATS

> Problemes de permisos 

> Problemes d'espai al labs

> Guies suposadament de la mateixa versió del symfony que no coincideix en res

> Guies oficials de symfony 3.4 que tot i seguir-les al peu de la lletra no funcionen bé

> Problemes amb la cau

> Problemes d'accés al entorn de producció a app.php pero no a app_dev.php. Impossible fer un rastreig de l'error doncs si intento canviar permisos el labs em dóna error.

## SOLUCIONS

![alt text](https://bytebucket.org/EduardValles/symfony/raw/433a0aae5cae14794ec56de940825aa77e916300/cache.png "SOLUCIO")

Tot i així, no he aconseguit arreglar tot, no sé si per algun tema de permisos al labs o mal preparament previ al desplegament a producció


## DIFERÈNCIES (o que haurien d'haver)

> No veure el panell de debug, es pot activar/desactivar amb arxiu de configuració: "toolbar: true/false"

> No hi ha accés a molts fitxers, sigui pel .htaccess o per diferents PHP que tenen el control d'entrada amb direcció local 127.0.0.1

